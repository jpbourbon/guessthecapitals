# Guess The Capitals

A simple PoC using Django 4 and python 3.9.9 of a capital-guessing game.

## Install
1 - Clone the repository to a suitable directory

2 - cd guessthecapitals

3 - use the requirements.txt to install dependencies either locally or in a virtual  environment

4 - run python src/manage.py runserver

5 - navigate to http://127.0.0.1:80000 in your browser

## Testing

Unit tests are located at src/tests.
Run python src/manage.py test src/tests

## Note

In order to not hit the api endpoint all the time and make data loading faster I implemented a simple caching system in src/capitals/modules/countries_client.py

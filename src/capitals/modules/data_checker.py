import random
from dataclasses import dataclass, field

from capitals.exceptions import NotFoundError
from capitals.modules.countries_client import get_data


@dataclass
class CountryResponse:
    id: int = -1,
    name: str = "",
    capital: str = "",


@dataclass
class MatchResponse:
    match: bool = False,
    user_capital: str = "",
    country: CountryResponse = CountryResponse(),


def get_country(country_id: int, data: dict[str, str] = get_data()) -> CountryResponse:
    """
        Returns a CountryResponse object from a given id and dataset

        Args:   country_id (int), data (json string)
        Raise:  NotFoundError
    """

    if country_id >= len(data["data"]):
        raise NotFoundError()

    return CountryResponse(
        id=country_id,
        name=data["data"][country_id]["name"],
        capital=data["data"][country_id]["capital"]
    )


def get_random_country() -> CountryResponse:
    """
        Returns a tuple with a random country id and name

        Args:   None
        Return: CountryResponse
    """

    data = get_data()
    count = len(data["data"])
    id = random.randrange(0, count - 1, 1) if count > 1 else 0

    return get_country(id, data)


def check_capital(country_id: int, capital: str) -> MatchResponse:
    """
        Compares the given country capitel to the stored data using the country id to locate it

        Args:   country_id (int), capital (str)
        Return: MatchResponse
    """

    data = get_data()

    response = MatchResponse(
        match=False,
        user_capital=capital,
        country=get_country(country_id=country_id, data=data)
    )

    if capital.lower() == response.country.capital.lower():
        response.match = True
        return response

    return response

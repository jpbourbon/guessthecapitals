import json
import time
from pathlib import Path

import requests
from capitals.exceptions import ApiConnectionError
from requests import ConnectionError, HTTPError, Timeout, api

ENDPOINT = "https://countriesnow.space/api/v0.1/countries/capital"
CACHE_FILE = Path("/var/tmp/countries_and_capitals.json")
CACHE_VALIDITY = 86400  # 1 day


def fetch_from_endpoint():
    """
        Fetches data from the external api

        Args:   None
        Return: Json with all countries and capitals
        Raise:  ApiConnectionError
    """

    try:
        resp = requests.get(ENDPOINT)
    except (HTTPError, Timeout, ConnectionError):
        raise ApiConnectionError()

    return resp.json()


def save_to_cache(data: str, path: Path):
    """
        Saves the json data fetched from the api endpoint to a file for faster access
        Args:   Json data, path to file
        Return: None
    """
    with open(path, 'w') as outfile:
        json.dump(data, outfile)


def read_from_cache(path: Path) -> str:
    """
        Reads the cached json data from a file

        Args:   path to file
        Return: Json data
    """
    with open(path, 'r') as json_file:
        return json.load(json_file)


def get_data() -> str:
    """
        Retrieves the data from the cache file or refreshes it if expired

        Args:   None
        Return: Json data
    """
    if not CACHE_FILE.exists() or CACHE_FILE.stat().st_mtime + CACHE_VALIDITY < time.time():
        data = fetch_from_endpoint()
        save_to_cache(data, CACHE_FILE)
        return data

    return read_from_cache(CACHE_FILE)

from django import forms

class CapitalsForm(forms.Form):
    capital = forms.CharField(required=True, max_length=30, label='')
    country_id = forms.IntegerField()
class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class NotFoundError(Error):
    """
        Exception raised for when an unexisting country id is given
    """

    def __init__(self, message="country id not valid") -> None:
        self.message = message


class ApiConnectionError(Error):
    """
        Raised when there is an error connecting to the external API
    """

    def __init__(self, message="Could not get data from external API") -> None:
        self.message = message

from django import forms
from django.http.response import HttpResponseServerError
from django.shortcuts import render
from django.template import loader
from django.core.handlers.wsgi import WSGIRequest

from capitals.forms import CapitalsForm
from capitals.modules.data_checker import (check_capital, get_country,
                                           get_random_country)


def _handle_get(request: WSGIRequest, id=-1):
    """
        Returns the index page for the app Capitals
        THe optional id argument is used to allow preserving the same
        country in case of an input validation error.

        Args:   WSGIRequest, id (int, default value -1)
        Return: The rendered template
    """

    if id is -1:
        country = get_random_country()
    else:
        country = get_country(country_id=id)

    form = CapitalsForm()
    form.initial['country_id'] = country.id
    form.fields['country_id'].widget = forms.HiddenInput()

    context = {
        'country_id': country.id,
        'country_name': country.name,
        'form': form,
    }

    return render(request, 'capitals/index.html', context)


def _handle_post(request: WSGIRequest):
    """
        Handles results POST request, returning the appropriate template according to the result

        Args:   WSGIRequest with country_id and capital as POST parameters
        Return: Success or Fail templates
    """

    form = CapitalsForm(request.POST)

    if not form.is_valid():
        clean_data = form.clean()
        country_id = clean_data.get("country_id")
        return _handle_get(request, country_id)

    else:
        clean_data = form.clean()
        country_id = clean_data.get("country_id")
        capital = clean_data.get("capital")
        result = check_capital(country_id=int(country_id), capital=capital)

        context = {
            'country_name': result.country.name,
            'user_capital': result.user_capital,
            'real_capital': result.country.capital,
        }

        if result.match:
            return render(request, "capitals/success.html", context)

        return render(request, "capitals/fail.html", context)


def index(request: WSGIRequest):
    """
        Boilerplate for the GET and POST requests.
        Redirects to the appropriate function
        Args:   WSGIRequest
        Returns: A template
    """

    if request.method == "POST":
        return _handle_post(request)

    return _handle_get(request)

from unittest import TestCase

from django.forms import fields

from capitals.forms import CapitalsForm


class CapitalsFormTestCase(TestCase):
    def test_capital_label_is_empty(self):
        """
            Test the label for the searchbox is empty so not rendered
        """

        form = CapitalsForm()

        self.assertTrue(form.fields['capital'].label is "")

    def test_capital_is_charfield(self):
        """
            Test the searchbox is of the charfield type
        """

        form = CapitalsForm()

        self.assertEquals(type(form.fields['capital']), fields.CharField)

    def test_country_id_is_integerfield(self):
        """
            Test the searchbox is of the integerfield type
        """

        form = CapitalsForm()

        self.assertEquals(type(form.fields['country_id']), fields.IntegerField)

from unittest.mock import patch

from django import forms
from django.test import Client, TestCase
from django.urls import reverse

from capitals.forms import CapitalsForm


@patch('capitals.modules.data_checker.get_data')
class HandleGetTestCase(TestCase):
    def test_handle_get_url_exists(self, mock_get_data):
        """
            Tests endpoint is reachable with GET
        """

        mock_get_data.return_value = {
            "data": [
                {"name": "Foo", "capital": "Bar"}
            ]
        }

        c = Client()
        response = c.get(path="/")

        self.assertEqual(response.status_code, 200)

    def test_handle_get_accessible_by_name(self, mock_get_data):
        """
            Tests endpoint is accessible by internal name
        """

        mock_get_data.return_value = {
            "data": [
                {"name": "Foo", "capital": "Bar"}
            ]
        }

        c = Client()
        response = c.get(path=reverse("index"))

        self.assertEqual(response.status_code, 200)

    def test_handle_get_use_right_template(self, mock_get_data):
        """
            Tests view uses the right template
        """

        mock_get_data.return_value = {
            "data": [
                {"name": "Foo", "capital": "Bar"}
            ]
        }

        c = Client()
        response = c.get(path=reverse("index"))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'capitals/index.html')

    def test_handle_get_context_complies(self, mock_get_data):
        """
            Tests context contains all keys
        """

        mock_get_data.return_value = {
            "data": [
                {"name": "Foo", "capital": "Bar"}
            ]
        }

        c = Client()
        response = c.get(path=reverse("index"))

        self.assertEqual(response.status_code, 200)
        self.assertEquals(response.context['country_id'], 0)
        self.assertEquals(response.context['country_name'], 'Foo')
        self.assertEquals(type(response.context['form']), CapitalsForm)

    def test_handle_get_changes_to_form(self, mock_get_data):
        """
            Tests form in context contains changes applied in the view
        """

        mock_get_data.return_value = {
            "data": [
                {"name": "Foo", "capital": "Bar"}
            ]
        }

        c = Client()
        response = c.get(path=reverse("index"))

        self.assertEqual(response.status_code, 200)
        self.assertEquals(type(response.context['form']), CapitalsForm)
        self.assertEquals(response.context['form'].initial['country_id'], 0)
        self.assertEquals(type(
            response.context['form'].fields['country_id'].widget),
            forms.widgets.HiddenInput
        )


@patch('capitals.modules.data_checker.get_data')
class HandlePostTestCase(TestCase):
    def test_handle_post_url_exists(self, mock_get_data):
        """
            Tests endpoint is reachable with POST
        """

        mock_get_data.return_value = {
            "data": [
                {"name": "Foo", "capital": "Bar"}
            ]
        }

        fake_data = {
            "country_id": 0,
            "capital": "Foo",
        }

        c = Client()
        response = c.post(path="/", data=fake_data)

        self.assertEqual(response.status_code, 200)

    def test_handle_post_accessible_by_name(self, mock_get_data):
        """
            Tests endpoint is accessible by internal name
        """

        mock_get_data.return_value = {
            "data": [
                {"name": "Foo", "capital": "Bar"}
            ]
        }

        fake_data = {
            "country_id": 0,
            "capital": "Foo",
        }

        c = Client()
        response = c.post(path=reverse("index"), data=fake_data)

        self.assertEqual(response.status_code, 200)

    def test_handle_post_use_right_template_success(self, mock_get_data):
        """
            Tests view uses the right template - success
        """

        mock_get_data.return_value = {
            "data": [
                {"name": "Foo", "capital": "Bar"}
            ]
        }

        fake_data = {
            "country_id": 0,
            "capital": "Bar",
        }

        c = Client()
        response = c.post(path=reverse("index"), data=fake_data)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'capitals/success.html')

    def test_handle_post_context_complies_match(self, mock_get_data):
        """
            Tests context contains all keys for a match
        """

        mock_get_data.return_value = {
            "data": [
                {"name": "Foo", "capital": "Bar"}
            ]
        }

        fake_data = {
            "country_id": 0,
            "capital": "Bar",
        }

        c = Client()
        response = c.post(path=reverse("index"), data=fake_data)

        self.assertEqual(response.status_code, 200)
        self.assertEquals(response.context['country_name'], 'Foo')
        self.assertEquals(response.context['user_capital'], 'Bar')
        self.assertEquals(response.context['real_capital'], 'Bar')

    def test_handle_post_use_right_template_fail(self, mock_get_data):
        """
            Tests view uses the right template - fail
        """

        mock_get_data.return_value = {
            "data": [
                {"name": "Foo", "capital": "Bar"}
            ]
        }

        fake_data = {
            "country_id": 0,
            "capital": "Foo",
        }

        c = Client()
        response = c.post(path=reverse("index"), data=fake_data)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'capitals/fail.html')

    def test_handle_post_context_complies_no_match(self, mock_get_data):
        """
            Tests context contains all keys for an unmatch
        """

        mock_get_data.return_value = {
            "data": [
                {"name": "Foo", "capital": "Bar"}
            ]
        }

        fake_data = {
            "country_id": 0,
            "capital": "Barrrr",
        }

        c = Client()
        response = c.post(path=reverse("index"), data=fake_data)

        self.assertEqual(response.status_code, 200)
        self.assertEquals(response.context['country_name'], 'Foo')
        self.assertEquals(response.context['user_capital'], 'Barrrr')
        self.assertEquals(response.context['real_capital'], 'Bar')

    def test_handle_post_validation_error(self, mock_get_data):
        """
            Tests validation error returns index template with same country not random
        """

        mock_get_data.return_value = {
            "data": [
                {"name": "Foo", "capital": "Bar"},
                {"name": "Foo1", "capital": "Bar1"},
                {"name": "Foo1", "capital": "Bar2"},
                {"name": "Foo3", "capital": "Bar3"},
            ]
        }

        fake_data = {
            "country_id": 0,
            "capital": " ",
        }

        c = Client()
        response = c.post(path=reverse("index"), data=fake_data)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'capitals/index.html')
        self.assertEquals(type(response.context['form']), CapitalsForm)
        self.assertEquals(response.context['form'].initial['country_id'], 0)
        self.assertEquals(type(
            response.context['form'].fields['country_id'].widget),
            forms.widgets.HiddenInput
        )

from os import utime
from pathlib import Path
from unittest import TestCase, mock

from capitals.exceptions import ApiConnectionError
from capitals.modules import countries_client


class FetchFromEndpointTestCase(TestCase):
    @mock.patch('requests.models.Response.json')
    @mock.patch('requests.get')
    def test_fetch_from_endpoint_success(self, mock_request, mock_response):
        """
            Test the request is done successfully
        """

        fake_json = "{\"foo\":\"bar\"}"

        mock_request.return_value = mock_response
        mock_response.return_value = fake_json

        method = countries_client.fetch_from_endpoint()

        assert mock_request.called_once()

    def test_fetch_from_endpoint_exception(self):
        """
            Test the exception raised for a broken endpoint connection
        """

        countries_client.ENDPOINT = "https://foo.bar.cuz"

        with self.assertRaises(ApiConnectionError):
            countries_client.fetch_from_endpoint()


class SaveToCacheTestCase(TestCase):
    def test_save_to_cache_success(self):
        """
            Tests the successful write of the given json data to a location in the disk to act as cache
        """

        fake_path = Path("/var/tmp/fake1.json")
        fake_json = "{\"foo\":\"bar\"}"

        assert fake_path.exists() == False

        countries_client.save_to_cache(data=fake_json, path=fake_path)

        assert fake_path.exists() == True

        fake_path.unlink()


class ReadFromCacheTestCase(TestCase):
    def test_read_from_cache_success(self):
        """
            Tests reading the cached data from the endpoint
        """

        fake_path = Path("/var/tmp/fake2.json")
        fake_json = "{\"foo\":\"bar\"}"

        countries_client.save_to_cache(data=fake_json, path=fake_path)

        json_response = countries_client.read_from_cache(path=fake_path)

        assert fake_json == json_response

        fake_path.unlink()


class GetDataTestCase(TestCase):
    def test_get_data_cache_exists(self):
        """
            Tests getting the data when the file exists and it's not expired
        """

        fake_path = Path("/var/tmp/fake3.json")
        fake_json = "{\"foo\":\"bar\"}"

        countries_client.save_to_cache(data=fake_json, path=fake_path)
        countries_client.CACHE_FILE = fake_path

        json_response = countries_client.get_data()

        assert fake_json == json_response

        fake_path.unlink()

    @mock.patch('capitals.modules.countries_client.read_from_cache')
    @mock.patch('capitals.modules.countries_client.save_to_cache')
    @mock.patch('requests.models.Response.json')
    @mock.patch('requests.get')
    def test_get_all_no_cache(self, mock_request, mock_response, mock_save_to_cache, mock_read_from_cache):
        """
            Tests getting the data when there is no cache file
        """

        fake_path = Path("/var/tmp/fake4.json")
        fake_json = "{\"foo\":\"bar\"}"

        mock_request.return_value = mock_response
        mock_response.return_value = fake_json

        countries_client.CACHE_FILE = fake_path

        countries_client.get_data()

        assert mock_save_to_cache.called_once()
        assert mock_read_from_cache.called_once()
        assert mock_request.called_once()

    @mock.patch('capitals.modules.countries_client.read_from_cache')
    @mock.patch('capitals.modules.countries_client.save_to_cache')
    @mock.patch('requests.models.Response.json')
    @mock.patch('requests.get')
    def test_get_all_expired_cache(self, mock_request, mock_response, mock_save_to_cache, mock_read_from_cache):
        """
            Tests getting the data when the cache file exists but has expired
        """

        fake_path = Path("/var/tmp/fake5.json")
        fake_json = "{\"foo\":\"bar\"}"

        mock_request.return_value = mock_response
        mock_response.return_value = fake_json

        fake_path.touch()
        utime(fake_path, (0, 0))
        countries_client.CACHE_FILE = fake_path

        countries_client.get_data()

        assert fake_path.exists()
        assert mock_save_to_cache.called_once()
        assert mock_read_from_cache.called_once()
        assert mock_request.called_once()

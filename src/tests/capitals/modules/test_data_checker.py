from unittest import TestCase, mock

from capitals.exceptions import NotFoundError
from capitals.modules import data_checker


class GetCountryTestCase(TestCase):
    def test_get_country_success(self):
        """
            Tests the method returns a specified country using the list key as id
        """

        fake_data = {
            "data": [
                {"name": "Foo", "capital": "Bar"}
            ]
        }

        response = data_checker.get_country(country_id=0, data=fake_data)

        assert type(response) == data_checker.CountryResponse
        assert response.id == 0

    def test_get_country_exception_not_found(self):
        """
            Tests the method raises a custom not found exception when the supplied key is not found
        """

        fake_data = {
            "data": []
        }

        with self.assertRaises(NotFoundError):
            data_checker.get_country(country_id=0, data=fake_data)


@mock.patch('capitals.modules.data_checker.get_data')
class GetRandomCountryTestCase(TestCase):
    def test_get_random_country_with_one_country(self, mock_get_data):
        """
            Tests getting a random country even if there is only one country in the list
        """

        mock_get_data.return_value = {
            "data": [
                {"name": "Foo", "capital": "Bar"}
            ]
        }

        response = data_checker.get_random_country()

        assert type(response) == data_checker.CountryResponse
        assert response.id == 0

    def test_get_random_country_with_many_countries(self, mock_get_data):
        """
            Tests the random country is gotten from a list of countries
        """

        mock_get_data.return_value = {
            "data": [
                {"name": "Foo", "capital": "Bar"},
                {"name": "Bar", "capital": "Foo"},
                {"name": "Cal", "capital": "Cuz"},
                {"name": "Cuz", "capital": "Cal"},
            ]
        }

        response = data_checker.get_random_country()

        assert type(response) == data_checker.CountryResponse
        assert response.id in range(0, 3)


@mock.patch('capitals.modules.data_checker.get_data')
class CheckCapitalTestCase(TestCase):
    def test_check_capital_matches(self, mock_get_data):
        """
            Tests the response for correct instance and the capital is correctly mactched
        """

        mock_get_data.return_value = {
            "data": [
                {"name": "Foo", "capital": "Bar"}
            ]
        }

        response = data_checker.check_capital(country_id=0, capital="Bar")

        assert type(response) == data_checker.MatchResponse
        assert response.match

    def test_check_capital_no_matches(self, mock_get_data):
        """
            Tests the capital is not the correct one
        """

        mock_get_data.return_value = {
            "data": [
                {"name": "Foo", "capital": "Bar"}
            ]
        }

        response = data_checker.check_capital(country_id=0, capital="Foo")

        assert type(response) == data_checker.MatchResponse
        assert response.match is False
